<?php

function gyu_sdk__inspect($file) {

	if(is_file($file)) {
		
		$path = basename($file);
		$ext = explode('.', $path);
		$appName = str_replace('.'.$ext[count($ext)-1], '', $path);

		$zip = zip_open($file);

		$isApp = false;
		$go = false;

		if ($zip) {
    		while ($zip_entry = zip_read($zip)) {
    			
    			$out["name"] = zip_entry_name($zip_entry);
    			$out["size"] = zip_entry_filesize($zip_entry);
    			$out["clearName"] = str_replace($appName, '', $out["name"]);

    			if($out["clearName"] == '/_/') {
    				$isApp = true;
    			}

        		$outside[] = $out;

    		}

    		zip_close($zip);

    		$go = true;

		}

		#if(count($outside) == 1 && $outside[0]["size"] > 0)
		#	return false;

		if(!$go)
			return false;

		$info["name"] = $appName;
		$info["filename"] = $file;
		$info["_"] = $isApp;

		if($isApp) {

			return $info;

		} else
			return false;

		
		#echo $file;

	}

}

function gyu_sdk__numAppToInstall() {
  $whereInstallers = absolute . 'app-setups/';
  $installers = glob($whereInstallers . '*.zip');
  foreach($installers as $file)
    if($ccnt = CallFunction('gyu_sdk', 'inspect', $file))
      $apps[] = $ccnt;

  return count($apps);
}

function gyu_sdk__download($url, $path) {

  $newfname = $path;
  $file = fopen ($url, "rb");
  if ($file) {
    $newf = fopen ($newfname, "wb");

    if ($newf)
    while(!feof($file)) {
      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
    }
  }

  if ($file) {
    fclose($file);
  }

  if ($newf) {
    fclose($newf);
  }
}

?>