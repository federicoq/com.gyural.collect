<?php

/**
 * Gyu SDK
 * The Software Development Kit for Gyural.
 *
 * This version, works only in CLI.
 * If you prefer a GUI one, use gyu_aSdk
 *
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class gyu_sdkCtrl extends standardController {
	
	var $index_tollerant = true;
	var $format;
	
	/**
	 * Controller for GET index.
	 */
	function GetIndex() {
		deb_error('Use gyu_sdk from CLI, otherwise install a GUI SDK, like gyu_aSdk', 1);
	}

	//////////////////
	// Version 1.10 //
	//////////////////

	/**
	 * Helper to create Assets File (Ctrl,Lib,Hooks,Funcs)
	 * @param  string $what       (ctrl,lib,hooks,funcs)
	 * @param  string $app        Application Name
	 * @param  string $controller ControllerName
	 * @param  array $args
	 * @return boolean
	 */
	function createAsset($what, $app, $controller, $args) {

		foreach($args as $k => $v) {
			$replace['{{'.$k.'}}'] = $v;
		}
		$keys = array_keys($replace);

		$tpl = file_get_contents(applicationCore . '/gyu_sdk/_sample/'.$what.'.php');

		$replacedContent = str_replace($keys, $replace, $tpl);

		$filePath = application . $app . '/_/' . $controller . '.'.$what.'.php';
		if(!is_file($filePath)) {
			file_put_contents($filePath, $replacedContent);
			return true;
		}
		else
			return false;

	}

	/**
	 * Main Action for CLI
	 */
	function CliRun() {

		$options = array(
			1 => 'Create a new Underscore App',
			2 => 'Create a new Controller (existing underscore)',
			3 => 'Create a new Lib (existing underscore)',
			4 => 'Create a new Funcs repository (existing underscore)',
			5 => 'Create a new Hook file (existing underscore)',
			6 => 'Dump an app',
			'exit' => 'Bang!'
		);

		$go = true;

		while($go == true) {

			echo $this->cli->string('Please, make a choice: ' . "\n");
			$action = $this->cli->ask($options);

			if($action == 'exit') {
				echo $this->cli->string(' BANG! ', 'white', 'yellow');
				$go = false;
			} else {

				echo $this->cli->separator(false);

				if($action == 1) {

					// Underscore APP - Creation

					echo $this->cli->string("Create a new UnderscoreApp", 'red');
					echo $this->cli->separator();

					$do = true;
					while($do) {

						$data = [];
						$ok = true;
						while($ok == true) {
							echo "Name of the app: ";
							$data["name"] = trim($this->cli->prompt());

							if(!$res = ApplicationDetail($data["name"])) {
								$ok = false;
							} else{
								if($res["user"] == true)
									echo $this->cli->string('Please, choice another name. This is already in use.' . "\n", 'red');
								else  {
									$ok = false;
									echo $this->cli->string('!! The selected name is already in use in the CORE. If you want to stop kill the process.' . "\n", 'yellow');
								}
								
							}

						}

						echo "Author: ";
						$data["author"] = trim($this->cli->prompt());

						echo "Combine letters to generate the code: \n";
						echo $this->cli->string('c', 'red') . 'trl' . "\n";
						echo $this->cli->string('l', 'red') . 'ib' . "\n";
						echo $this->cli->string('f', 'red') . 'uncs' . "\n";
						echo $this->cli->string('h', 'red') . 'ooks' . "\n";

						$data["gen"] = array(
							'c' => 0,
							'l' => 0,
							'f' => 0,
							'h' => 0
						);

						echo 'Your choice: ';
						$sel = trim($this->cli->prompt());

						for($i = 0; $i < strlen($sel); $i++)
							$data["gen"][$sel[$i]] = 1;

						echo $this->cli->separator();
						echo "Please, confirm this infos:\n";
						echo "Application Name: " . $this->cli->string($data["name"], 'green') . "\n";
						echo "Author: " . $this->cli->string($data["author"], 'green') . "\n";
						echo "Ctrl: " .  ($data["gen"]["c"] == 1 ? $this->cli->string('yes', 'green') : $this->cli->string('no', 'red')) . "\n";
						echo "Lib: " .   ($data["gen"]["l"] == 1 ? $this->cli->string('yes', 'green') : $this->cli->string('no', 'red')) . "\n";
						echo "Funcs: " . ($data["gen"]["f"] == 1 ? $this->cli->string('yes', 'green') : $this->cli->string('no', 'red')) . "\n";
						echo "Hooks: " . ($data["gen"]["h"] == 1 ? $this->cli->string('yes', 'green') : $this->cli->string('no', 'red')) . "\n";
						
						echo $this->cli->separator(false);
						echo "Is that correct? ";
						$reply = $this->cli->ask(['yes', 'no']);

						if($reply == 'yes') {
							$do = false;
						} else {
							echo "\nOK. Start Over.";
							echo $this->cli->separator(false);
						}

					}

					echo $this->cli->separator();

					$this->cli->progressBar(0,100, 'Process');

					$directory = application . $data["name"];

					if(mkdir($directory)) {

						$this->cli->progressBar(5,100, 'Process');

						mkdir($directory . '/_');
						$this->cli->progressBar(10,100, 'Process');

						$data["version"] = '0.1';
						$data["ctrl"] = $data["name"];

						if($data["gen"]["c"]) {
							$this->createAsset('ctrl', $data["name"], $data["name"], $data);
							$this->cli->progressBar(20,100, 'Process');
							sleep(1);
						}

						if($data["gen"]["l"]) {
							$this->createAsset('lib', $data["name"], $data["name"], $data);
							$this->cli->progressBar(40,100, 'Process');
							sleep(1);
						}

						if($data["gen"]["f"]) {
							$this->createAsset('funcs', $data["name"], $data["name"], $data);
							$this->cli->progressBar(60,100, 'Process');
							sleep(1);
						}

						if($data["gen"]["h"]) {
							$this->createAsset('hooks', $data["name"], $data["name"], $data);
							$this->cli->progressBar(80,100, 'Process');
							sleep(1);
						}

						$this->cli->progressBar(100,100, 'Process');
						echo "The application " . $this->cli->string($data["name"], 'green') . " created! Yeaah!";
						echo $this->cli->separator();

					}

				} else if($action < 6) {

					// This will controll all other options (till 6, Ctrl,Lib,Funcs,Hooks)

					$selector = [
						2 => ['Controller', 'SubController', 'ctrl'],
						3 => ['Library', 'SubLibrary', 'lib'],
						4 => ['Functions Repository', 'SubFunctions', 'funcs'],
						5 => ['Hooks', 'Hooks', 'hooks']
					];

					echo $this->cli->string("Add a ".$selector[$action][0]." to an existing UnderscoreApp", 'red');
					echo $this->cli->separator();

					$do = true;
					while($do) {
						echo 'Type the name of the UnderscoreApp: ';
						$appName = trim($this->cli->prompt());

						if($res = ApplicationDetail($appName)) {
							if($res["user"] == false) {
								$do = true;
								echo $this->cli->string('You can\'t add an ASSET to a CORE application. Try Again!' . "\n", 'red');
							} else
								$do = false;
						} else {
							echo $this->cli->string('Application called `'.$appName.'` not found. Try Again!' . "\n", 'yellow');
						}

					}

					echo 'UnderscoreApp: ' . $this->cli->string($appName, 'green') . " found.\n\n";
					echo 'Type the name of the '.$selector[$action][1].' (leave empty if you want to create the main): ' . $this->cli->string($appName . '_', 'purple');
					
					$name = trim($this->cli->prompt());

					echo "Set the Author name (or leave empty): ";
					$args["author"] = trim($this->cli->prompt());
					if(strlen($args["author"]) == 0)
						$args["author"] = 'Gyural Superhero!';

					echo "Set the version (or leave empty): ";
					$args["version"] = trim($this->cli->prompt());
					if(strlen($args["version"]) == 0)
						$args["author"] = '0.1';

					$done = true;
					while($done) {
						
						$this->cli->separator();

						echo	"Confirm the creation of the " . 
								$selector[$action][1].": " . $this->cli->string($name, 'purple') . 
								" => " . $this->cli->string($appName . (strlen($name) > 0 ? '_' . $name : '') . '.ctrl.php', 'green') . " ?\n";

						if($this->cli->ask(array('yes', 'no')) == 'yes') {
							$args["ctrl"] = $appName . (strlen($name) > 0 ? '_' . $name : '');
							$args["name"] = $appName;
							if($this->createAsset($selector[$action][2], $appName, $appName . (strlen($name) > 0 ? '_' . $name : ''), $args)) {
								echo $this->cli->string($selector[$action][0] . ' created', 'green');
								$done = false;
							} else {
								echo $this->cli->string('Impossible to create ' . $selector[$action][0] . '.', 'red');
							}
						}

					}

					echo $this->cli->separator();
					echo "Press enter to continue.\n";
					echo $this->cli->prompt();

					echo $this->cli->separator(false);

				}

			}

		}

	}

	/**
	 * Tiny Help.
	 */
	function CliHelp() {

		$apps = deb_installed_app();

		echo "List of available CLI methods: ";
		echo $this->cli->separator();

		foreach($apps as $app) {
			if($app["detail"]["isUnderscore"]) {

				if(is_file($app["detail"]["dir"] . '/_/' . $app["name"] . '.ctrl.php')) {
					LoadApp($app["name"]);
					$ctrl = $app["name"] . 'Ctrl';
					$actions = get_class_methods($ctrl);

					foreach($actions as $act) {
						if(strstr($act, 'Cli')) {
							$help[$app["name"]][] = strtolower(str_replace('Cli', '', $act));
						}
					}

				}
			}
		}

		foreach($help as $k => $actions) {
			echo $this->cli->string($k, 'green', null) . "\n";

			foreach($actions as $a) {
				echo "\t" . $a . " ";
				echo $this->cli->string("`php cli.php $k $a`", 'red', null) . "\n";
			}

		}

	}

	/**
	 * Code Prototype(r)
	 */
	function CliGanja() {

		$eval = [];
		$a = true;

		echo	'Hello Man, welcome into Ganja! You can protoType a portion of code.' . "\n" . 
				'Use this console as a tiny Gyural Enviroment' . 
				$this->cli->separator() . 
				"(type `exit` to exit)" .
				$this->cli->separator();

		while($a == true) {

			$tmp = trim($this->cli->prompt());
			if($tmp !== 'exit') {
				$eval[] = rtrim($tmp, ';') . ';';
			} else {
				$a = false;
			}

			eval(implode(";", $eval));

		}

		echo $this->cli->separator(false);
		echo "Well done myfriend! :)";

		echo $this->cli->separator();
		echo "Would you like to save the file?";

		$r = $this->cli->ask(['yes', 'no']);
		if($r == 'yes') {
			$fileName = date('d-m-y.h-i-s') . '.php';
			file_put_contents(cache . $fileName, '<?' . "\n" . implode("\n", $eval) . "\n");
			echo 'File stored at: ' . $this->cli->string('/cdn/cache/' . $fileName, 'green', null) . "\n";
		}

		echo "\nHave a nice day :)";

		echo $this->cli->separator(false);

	}

	/**
	 * List of Installed Application
	 * @param boolean $routes if true, we also print the Methods.
	 */
	function CliApplications($routes = false) {

		echo "Type `gyu_sdk applications routes` for also routes.";
		echo $this->cli->separator();

		$applications = deb_installed_app();
		$i = 0;
		foreach($applications as $app) {
			$data[$i] = array(
				'Application' => $app["name"],
				'Underscore' => ($app["detail"]["isUnderscore"] ? 'yes' : 'no'),
				'Core' => ($app["detail"]["user"] == true ? 'no' : 'yes')
			);
			$i++;

			if($routes) {
				$data[($i-1)]["Route"] = '###';

				$e = LoadApp($app["name"]);
				if($e) {
					$valid = [];
					$methods = get_class_methods($app["name"] . 'Ctrl');
					foreach($methods as $method) {
						foreach($this->methods_prefix as $pfx) {
							if(stripos($method, $pfx) === 0) {
								$data[$i] = [
									'Application' => '',
									'Underscore' => '',
									'Core' => '',
									'Route' => $method,
								];
								$valid[] = $method;
								$i++;
							}
						}
					}
					#print_r($valid);
				}

			}

			
		}

		echo $this->cli->draw_text_table($data);

	}

}