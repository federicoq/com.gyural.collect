<?php

/**
 * {{name}} - Hooks
 *
 *
 * @version {{version}}
 * @author {{author}}
 */

\Gyu\Hooks::set('gyu.created', '{{ctrl}}_sample_hook');

function {{ctrl}}_sample_hook($time) {

	// this is a sample hook!

}