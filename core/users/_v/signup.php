<?

$this->view('error-console');

echo \Gyu\Form::open();
echo \Gyu\Form::input('username', $this->content["username"], ['placeholder' => 'Username']);
echo \Gyu\Form::password('password', null, ['placeholder' => 'Password']);
echo \Gyu\Form::password('gyu_password', null, ['placeholder' => 'Repeat your password']);
echo \Gyu\Form::submit('gyu_login', 'Register');
echo \Gyu\Form::close();

$this->view('footer');

?>