<div class="row">
	<div class="medium-9 column hide-for-small"></div>
	<div class="medium-3 column">
		<?
			$this->view('error-console');
			echo \Gyu\Form::open();
			echo \Gyu\Form::input('username', $this->content["username"], ['placeholder' => 'Username']);
			echo \Gyu\Form::password('password', null, ['placeholder' => 'Password']);
			echo \Gyu\Form::submit('gyu_login', 'Login');
			echo \Gyu\Form::close();
			$this->view('footer');
		?>
	</div>
</div>