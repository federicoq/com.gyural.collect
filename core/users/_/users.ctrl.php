<?php

/**
 * Users - Default Controller
 *
 * Note: if you'r going to update a previous version of gyural and keep your users:
 * - Update password field in db, make it varchar(255) and then add "@" at the end of each user. [1 - why]
 * - Remove the /app/users and replace /funcs/autoload/security.php with the 1.5's one.
 *
 * 1 - Why
 * =======
 * 
 * This because the login mechanism is now changed, each user's password have a structure like:
 * passwordsalted@salt
 * the salt is a random string that will be used with the user input password to generate the "passwordsalted".
 * see /app/users/_/users.lib.php and the setPassword method. 
 *
 * @version 1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class usersCtrl extends standardController {

	var $index_tollerant = false;

	/**
	 * Set to true to enable registrations
	 * @var boolean
	 */
	var $registrations_open = false;

	/**
	 * Url for the registration form
	 * @var string
	 */
	var $registrations_url = '/GetSignup';
	
	/**
	 * Default group for new users
	 * @var integer
	 */
	var $registrations_group = 0;

	/**
	 * Activation is required?
	 * @var boolean
	 */
	var $registrations_activation = true;

	/**
	 * Sample Email for new accounts
	 * @var string
	 */
	var $registrations_activation_email = 'Hello boy, click on the link to activate your account!<br />';

	/**
	 * Enable user to recover the password
	 * @var boolean
	 */
	var $password_recovery = true;

	/**
	 * Url for the recovery password
	 * @var string
	 */
	var $password_url = '/GetRecovery';

	/**
	 * Sample Email for recovery mail
	 * @var string
	 */
	var $recovery_email = 'Hello boy, recover your password!<br />';

	/**
	 * Url for Login
	 * @var string
	 */
	var $noLogin_uri = '/GetLogin';

	/**
	 * Home for registered.
	 * @var string
	 */
	var $dashboard = '/GetMe';


	/**
	 * Construct for register the filters.
	 */
	function __construct() {

		$this->gyuFilter('before', 'users.canSignup', 1, $this->_base(1) . $this->noLogin_uri, ['GetSignup', 'PostSignup', 'ApiSignup', 'GetActive', 'ApiActive']);
		$this->gyuFilter('before', 'users.canLogin', 1, $this->_base(1) . $this->dashboard, ['GetLogin', 'PostLogin', 'ApiLogin']);
		$this->gyuFilter('before', 'users.logged', 1, $this->_base(1) . $this->noLogin_uri, ['GetMe', 'GetLogout', 'ApiMe']);
		$this->gyuFilter('before', function() { return !logged() && $this->password_recovery ? false:true; }, 1, $this->_base(1) . $this->noLogin_uri, ['GetRecovery']);

	}

	// -------------------------------------------------
	
	/**
	 * Filter: can see signup pages?
	 * 
	 * @return mixed
	 */
	function canSignup() {

		if($this->registrations_open === true && !logged())
			return false;
		else if($this->registrations_open == false)
			return ['error' => 'Registrations are closed.'];
		else
			return ['error' => 'Your alredy logged.'];

	}

	/**
	 * Filter: is user logged?
	 * 
	 * @return mixed
	 */
	function logged() {

		if(logged())
			return false;
		return ['error' => 'Your not logged'];

	}

	/**
	 * Filter: can user login?
	 * 
	 * @return mixed
	 */
	function canLogin() {

		if(!logged())
			return false;
		return ['error' => 'Already logged.'];

	}

	// -------------------------------------------------

	/**
	 * GetSignup
	 */
	function GetSignup() {
		$this->view('signup');
	}

	/**
	 * PostSignup
	 */
	function PostSignup() {

		$register = $this->ApiSignup($username = $_REQUEST["username"], $password = $_REQUEST["password"], $confirm = $_REQUEST["gyu_password"]);
		if(is_object($register)) {
			if($this->registrations_activation == true)
				$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Please click on the link in the email.']);
			else
				$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Log with your credential.']);
		} else
			$this->move($this->_base(1) . $this->registrations_url, $register);

	}

	/**
	 * ApiSignup
	 * 
	 * @param string $username
	 * @param string $password
	 * @param string $confirm
	 */
	function ApiSignup($username, $password, $confirm) {

		$toValidate = [
			'username' => $username,
			'password' => $password,
			'gyu_password' => $confirm
		];

		$validation = new \Gyu\Validation($toValidate);
		$validation->set_rules('username', 'Username', 'is_unique[users.username]|min_length[5]|valid_email', ['is_unique' => 'Already Registered', 'min_length' => 'At least 5 char', 'valid_email' => 'Use an email as username']);
		$validation->set_rules('password', 'Password', 'required|min_length[5]', ['min_length' => 'At least 5 char']);
		$validation->set_rules('gyu_password', 'Password Confirm', 'required|matches[password]', ['matches' => 'Password confirmation not match.']);

		if($validation->run()) {

			$user = LoadClass('users', 1);
			$user->setAttr('username', $toValidate['username']);
			$user->setAttr('password', $toValidate['password']);
			$user->setAttr('group', $this->registrations_group);

			if($this->registrations_activation)
				$user->setAttr('active', 0);
			else
				$user->setAttr('active', 1);

			if($user->hangExecute()) {

				if($this->registrations_activation == true) {
					$activationCode = CallFunction('strings', 'random', 10);
					CallFunction(
						'mail', 
						'save', 
						$user->username, 
						'['.siteName.'] Activation Code', 
						$this->registrations_activation_email . 
						'<a href="' . uri . '/'. $this->_base() . '/active/token:' . $activationCode . '/user_id:' . $user->id . '">Activate your account</a>'
					);
					CallFunction('vars','set', 'activationCode', $activationCode, $user->id);
				}

				return $user;

			} else 
				return ['error' => 'Impossible to create the user..', 'content' => $validation->validation_data];

		} else {
			return ['error' => implode('<br />', $validation->error_array()), 'content' => $validation->validation_data];
		}

	}

	/**
	 * GetActive
	 */
	function GetActive() {

		if(is_object($active = $this->ApiActive($token = $_REQUEST["token"], $user_id = $_REQUEST["user_id"])))
			$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Please, log in']);
		else
			$this->move($this->_base(1) . $this->noLogin_uri, $active);

	}

	/**
	 * ApiActive
	 * 
	 * @param string $token
	 * @param string $user_id
	 */
	function ApiActive($token = false, $user_id = false) {

		if(!$token)
			$token = $_REQUEST["token"];
		if(!$user_id)
			$user_id = $_REQUEST["user_id"];

		$activation = CallFunction('vars', 'get', 'activationCode', $user_id);
		if($activation == $token) {
			$user = LoadClass('users', 1)->get($user_id);
			if($user) {
				if($user->active == 0) {
					$user->setAttr('active', 1);
					$user->putExecute();
					return $user;
				} else {
					return ['error' => 'Already Activated'];
				}
			} else {
				return ['error' => 'User not found'];
			}
		} else {
			return ['error' => 'Invalid activation key'];
		}

	}

	// -------------------------------------------------

	/**
	 * GetRecovery
	 */
	function GetRecovery() {
		$this->view('recovery');
	}

	/**
	 * PostRecovery
	 */
	function PostRecovery() {

		if(is_object($recovery = $this->ApiRecovery($_REQUEST["username"])))
			$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Check your email']);
		else
			$this->move($this->_base(1) . $this->password_url, $recovery);
	}

	/**
	 * ApiRecovery
	 * 
	 * @param string $username
	 */
	function ApiRecovery($username) {

		$toValidate = [
			'username' => $username
		];

		$validation = new \Gyu\Validation(array('username' => $username));
		$validation->set_rules('username', 'Username', 'required|valid_email', ['required' => 'Please insert your email', 'valid_email' => 'Insert a valid email']);

		if($validation->run()) {

			$user = LoadClass('users', 1)->filter(array('username', $toValidate['username']), 'ONE');
			if($user) {
				$restoreCode = CallFunction('strings', 'random', 10);
				CallFunction('vars', 'set', 'restoreCode', $restoreCode, $user->id);
				CallFunction(
					'mail', 
					'save', 
					$user->username,
					'['.siteName.'] Recovery Password', 
					$this->recovery_email . 
					'<a href="' . uri . '/'. $this->_base() . '/recoveryset/token:' . $restoreCode . '/user_id:' . $user->id . '">Recover your password</a>'
				);
				return $user;
			} else {
				return ['error' => 'Email not found.', 'content' => $validation->validation_data];
			}

		} else
			return ['error' => implode('<br />', $validation->error_array()), 'content' => $validation->validation_data];

	}

	/**
	 * GetRecoverySet
	 */
	function GetRecoverySet() {

		$aspectedToken = CallFunction('vars', 'get', 'restoreCode', $_REQUEST["user_id"]);
		if($aspectedToken == $_REQUEST["token"]) {
			$this->user = LoadClass('users', 1)->get($_REQUEST["user_id"]);
			$this->view('new-password');
		} else {
			$this->move($this->_base(1) . $this->password_url, ['error' => 'Invalid Token']);
		}

	}

	/**
	 * PostRecoverySet
	 */
	function PostRecoverySet() {
		if(is_object($recovery = $this->ApiRecoverySet($user_id = $_REQUEST["user_id"], $password = $_REQUEST["password"], $confirm = $_REQUEST["gyu_password"], $token = $_REQUEST["token"]))) {
			$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Please, login']);
		} else {
			$this->error = $recovery['error'];
			$this->view('new-password');
		}
	}

	/**
	 * ApiRecoverySet
	 * 
	 * @param int $user_id
	 * @param string $password
	 * @param string $confirm
	 * @param string $token
	 */
	function ApiRecoverySet($user_id, $password, $confirm, $token) {

		$toValidate = [
			'password' => $password,
			'gyu_password' => $confirm
		];

		$validate = new \Gyu\Validation($toValidate);
		$validate->set_rules('password', 'P', 'required|min_length[5]', ['min_length' => 'At least 5 char!']);
		$validate->set_rules('gyu_password', 'P', 'required|matches[password]', ['matches' => 'Missmatch.']);

		if($validate->run()) {
			$aspectedToken = CallFunction('vars', 'get', 'restoreCode', $user_id);
			if($aspectedToken == $token) {
				$user = LoadClass('users', 1)->get($user_id);
				$user->setAttr('password', $password);
				if($user->putExecute()) {
					$aspectedToken = CallFunction('vars', 'del', 'restoreCode', $user_id);
					return $user;
				} else
					return ['error' => 'Impossible update your account..'];
			} else
				return ['error' => 'Token not found.'];
		} else
			return ['error' => implode('<br />', $validate->error_array())];

	}


	// -------------------------------------------------
	
	/**
	 * GetLogin
	 */
	function GetLogin() {
		$this->view('login');
	}

	/**
	 * PostLogin
	 */
	function PostLogin() {

		$login = $this->ApiLogin($_POST["username"], $_POST["password"]);
		if(is_object($login))
			$this->move($this->_base(1) . $this->dashboard);
		else
			$this->move($this->_base(1) . $this->noLogin_uri, $login);

	}

	/**
	 * ApiLogin
	 * 
	 * @param string $username
	 * @param string $password
	 */
	function ApiLogin($username = false, $password = false) {

		if(false === $username)
			$username = $_REQUEST["username"];
		if(false === $password)
			$password = $_REQUEST["password"];

		$toValidate = [
			'username' => $username,
			'password' => $password
		];

		$validation = new \Gyu\Validation($toValidate);
		$validation->set_rules('username', 'Username', 'required|min_length[5]', ['required' => 'Username Required', 'min_length' => 'At least 5 chars.']);
		$validation->set_rules('password', 'Password', 'required', ['required' => 'Password Required']);

		if($validation->run()) {

			$user = LoadClass('users', 1)->_try($username, $password);
			if($user) {
				$_SESSION["login"] = $user;
				return $user;
			}
			else {
				$this->ApiLogout();
				return ['error' => 'Wrong username or password', 'content' => $validation->validation_data];
			}


		} return ['content' => $validation->validation_data, 'error' => implode('<br />', $validation->error_array())];

	}

	/**
	 * GetLogout
	 */
	function GetLogout() {
		$this->ApiLogout();
		$this->move($this->_base(1) . $this->noLogin_uri, ['error' => 'Logout']);
	}

	/**
	 * ApiLogout
	 */
	function ApiLogout() {
		unset($_SESSION["login"]);
		return true;
	}


	// -------------------------------------------------

	/**
	 * GetMe
	 */
	function GetMe() {
		$this->view('me');
	}

	/**
	 * ApiMe
	 */
	function ApiMe() {
		return Me();
	}

	/**
	 * ApiSample
	 */
	function ApiSample() {

		if(!isset($_REQUEST["u"]) || !isset($_REQUEST["p"]))
			return 'Specify the username and password.. ($_REQUEST[u] and $_REQUEST[p])';

		$user = LoadClass('users', 1);
		$user->setAttr('username', $_REQUEST["u"]);
		$user->setAttr('password', $_REQUEST["p"]);

		return array($user, $user->hang());

	}

}
