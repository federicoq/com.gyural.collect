<?php

/**
 * Users - Hooks
 *
 * @version 1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

\Gyu\Hooks::set('gyu.db.ready', 'users_hook__login');
\Gyu\Hooks::set('gyu.after-libs', 'users_hook__init');

function users_hook__init($time = null) {
	LoadClass('users');
}

function users_hook__login($time = null) {
	if(logged()) {
		if(!$_SESSION["login"]->_refresh())
			unset($_SESSION["login"]);
		$tried = true;
	}
}
