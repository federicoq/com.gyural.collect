<?php

/**
 * GYU - Cli
 *
 * Helper to manage execution from Command Line Interface.
 *
 * @version 0.1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

namespace Gyu;

class Cli {

	/**
	 * Array of available foreground colors
	 * @var array
	 */
	private $foreground_colors = array();

	/**
	 * Array of available background colors
	 * @var array
	 */
	private $background_colors = array();

	private $bars = array();

	/**
	 * Initialize the Lib, fill the colors arrays
	 */
	public function __construct() {
		// Set up shell colors
		$this->foreground_colors['black'] = '0;30';
		$this->foreground_colors['dark_gray'] = '1;30';
		$this->foreground_colors['blue'] = '0;34';
		$this->foreground_colors['light_blue'] = '1;34';
		$this->foreground_colors['green'] = '0;32';
		$this->foreground_colors['light_green'] = '1;32';
		$this->foreground_colors['cyan'] = '0;36';
		$this->foreground_colors['light_cyan'] = '1;36';
		$this->foreground_colors['red'] = '0;31';
		$this->foreground_colors['light_red'] = '1;31';
		$this->foreground_colors['purple'] = '0;35';
		$this->foreground_colors['light_purple'] = '1;35';
		$this->foreground_colors['brown'] = '0;33';
		$this->foreground_colors['yellow'] = '1;33';
		$this->foreground_colors['light_gray'] = '0;37';
		$this->foreground_colors['white'] = '1;37';

		$this->background_colors['black'] = '40';
		$this->background_colors['red'] = '41';
		$this->background_colors['green'] = '42';
		$this->background_colors['yellow'] = '43';
		$this->background_colors['blue'] = '44';
		$this->background_colors['magenta'] = '45';
		$this->background_colors['cyan'] = '46';
		$this->background_colors['light_gray'] = '47';
	}

	/**
	 * Return string stylized
	 * @param  string $string           Plain text string
	 * @param  string $foreground_color Foreground Color
	 * @param  string $background_color Background Color
	 * @return string                   
	 */
	public function string($string, $foreground_color = null, $background_color = null) {
		$colored_string = "";
		// Check if given foreground color found
		if (isset($this->foreground_colors[$foreground_color])) {
			$colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
		}
		// Check if given background color found
		if (isset($this->background_colors[$background_color])) {
			$colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
		}

		// Add string and end coloring
		$colored_string .=  $string . "\033[0m";
		return $colored_string;
	}

	/**
	 * Retrive user input
	 * @return string
	 */
	public function prompt() {

		$handle = fopen ("php://stdin","r");
		$line = fgets($handle);
		return $line;
		
	}

	/**
	 * Give to user a list of choice.
	 * If the user type something different will be asked again for input
	 * 
	 * @param  array $replies List of possibilities
	 * @return string
	 */
	public function ask($replies) {

		$find = false;

		if(!$this->isAssoc($replies)) {

			while($find == false) {
				echo "\n" . '['.implode(', ', $replies).']: ';
				$r = trim($this->prompt());
				if(in_array($r, $replies))
					return $r;
			}

		} else {

			while($find == false) {
				foreach($replies as $k => $v)
					echo $this->string($k, 'green') . ': ' . $v . "\n";
				
				echo 'Your choice: ';
				$r = trim($this->prompt());
				if(isset($replies[$r]))
					return $r;
			}

		}

	}

	/**
	 * Print Progress Bar
	 * @param  integer $current
	 * @param  integer $total
	 * @param  string  $label
	 * @param  integer $size
	 */
	function progressBar($current=0, $total=100, $label="", $size=50) { 

	    //Don't have to call $current=0 
	    //Bar status is stored between calls 
	    if(!isset($this->bars[$label])) { 
	        $new_bar = TRUE; 
	        fputs(STDOUT,"$label Progress:\n"); 
	    } 
	    if($current == $this->bars[$label]) return 0; 

	    $perc = round(($current/$total)*100,2);        //Percentage round off for a more clean, consistent look 
	    for($i=strlen($perc); $i<=4; $i++) $perc = ' '.$perc;    // percent indicator must be four characters, if shorter, add some spaces 

	    $total_size = $size + $i + 3; 
	    // if it's not first go, remove the previous bar 
	    if(!$new_bar) { 
	        for($place = $total_size; $place > 0; $place--) echo "\x08";    // echo a backspace (hex:08) to remove the previous character 
	    } 
	     
	    $this->bars[$label]=$current; //saves bar status for next call 
	    // output the progess bar as it should be 
	    for($place = 0; $place <= $size; $place++) { 
	        if($place <= ($current / $total * $size)) echo $this->string(' ', 'green', 'green');    // output green spaces if we're finished through this point 
	        else echo $this->string(' ', 'gray', 'gray');                    // or grey spaces if not 
	    } 

	    // end a bar with a percent indicator 
	    echo " $perc%"; 

	    if($current == $total) { 
	        echo "\n";        // if it's the end, add a new line 
	        unset($this->bars[$label]); 
	    } 
	}

	/**
	 * Print a visual separator.
	 * @param  boolean $silent If false, we also return - - -
	 * @return string
	 */
	function separator($silent = true) {
		if($silent == true)
			return "\n\n";
		else
			return "\n\n- - -\n\n";
	}

	/**
	 * Check if an array is Associative
	 * @param  array  $arr
	 * @return boolean
	 */
	function isAssoc($arr) {
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	/**
	 * Print an array as a PLAIN TEXT Table.
	 * @param  array $table
	 * @return string
	 */
	function draw_text_table($table, $plain = false) {

    	// Work out max lengths of each cell
		foreach ($table AS $row) {
			$cell_count = 0;
			
			foreach ($row AS $key=>$cell) {
				$cellKeys[$key] = $cell;
				$cell_length = strlen($cell);
				$cell_count++;
				if (!isset($cell_lengths[$key]) || $cell_length > $cell_lengths[$key]) $cell_lengths[$key] = $cell_length;
			}
    	}

    	foreach($cellKeys as $key => $cell) {
    		if(strlen($key) > $cell_lengths[$key])
    			$cell_lengths[$key] = strlen($key);
    	}
		
		// Build header bar
		$bar = '+';
		$header = '|';
		$i=0;
		
		foreach ($cell_lengths AS $fieldname => $length) {
			$i++;
			$bar .= str_pad('', $length+2, '-')."+";
			
			$name = $fieldname;
			if (strlen($name) > $length) {
				// crop long headings
				$name = substr($name, 0, $length-1);
        	}
        	if($plain)
        		$header .= ' '.str_pad($name, $length, ' ', STR_PAD_RIGHT) . " |";
        	else
				$header .= ' '.$this->string(str_pad($name, $length, ' ', STR_PAD_RIGHT), 'purple') . " |";
    	}
		
		$output = '';
		$output .= $bar."\n";
		$output .= $header."\n";
		$output .= $bar."\n";

		// Draw rows
		foreach ($table AS $row) {
			$output .= "|";
			foreach ($row AS $key=>$cell) {
				if($cell == '###') {
					$cell = '';
					$ch = '-';
				}
				else
					$ch = ' ';
				$output .= ' '.str_pad($cell, $cell_lengths[$key], $ch, STR_PAD_RIGHT) . " |";
			}
			$output .= "\n";
		}

		$output .= $bar."\n";
		return $output;
	}
}