<?php

/**
 * GYU - Config
 *
 * Config Manager
 *
 * @version 0.1
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

namespace Gyu;

class Config {

	function __construct($name) {

		$type = strstr($name, '\\') ? 1 : 0;

		if($type == 1)
			$file = absolute . 'sys/' . str_replace("\\", "/", $name) . '.json';
		else {

			list($tA) = explode('/', $name);

			if($isThatApp = ApplicationDetail($tA))
				$file = $isThatApp["dir"] . '/_/' . str_replace('/', '_', $name). '.config.json';
		
		}

		if(is_file($file)) {
			$this->path = $file;
			$this->params = static::loadJSON($file);
		}

	}

	function get($key) {
		return $this->params[$key];
	}

	function set($key, $value) {
		$this->params[$key] = $value;
	}

	function save() {
		$r = json_encode($this->params);
		file_put_contents($this->path, $r);
		return $this;
	}

	function loadJSON($f) {
		return json_decode(file_get_contents($f), 1);
	}

}