<?php

$app_data["head"]["title"] = $app_data["head"]["title"] ? $app_data["head"]["title"] : siteName;
$app_data["head"]["description"] = $app_data["head"]["description"] ? $app_data["head"]["description"] : siteName;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><? echo $app_data["head"]["title"]; ?></title>
	<link rel="stylesheet" type="text/css" href="//cdn.foundation5.zurb.com/foundation.css">
	<style type="text/css">
		code { background-color: transparent; border-color: transparent; border-style: none; }</style>
</head>
<body style="margin-top: 1em">

	<div class="row">
		<div class="medium-3 column">
			<h3 class="subheader">Gyural <? echo version; ?></h3>
			<hr />
			<h5><? echo $app_data["head"]["title"]; ?></h5>
		</div>
		<div class="medium-9 column">