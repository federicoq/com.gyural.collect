<?
echo '<h1>' . $this->title . '</h1>';

echo \Gyu\Form::open();

echo $this->errors ? '<p>' . implode('<br />', $this->errors) . '</p>' : '';

echo \Gyu\Form::input('name', $this->content["name"]);
echo \Gyu\Form::submit('gyu_submit', $this->action);
echo \Gyu\Form::close();

?>