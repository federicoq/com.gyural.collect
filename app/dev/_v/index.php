<? 

$this->view('header', $this->title);
$this->view('menu', ['title' => 'Vendors', 'menu' => $this->sections_vendor]);
$this->view('menu', ['title' => 'Patterns', 'menu' => $this->sections_patterns]);
$this->view('footer', $this->title);

?>