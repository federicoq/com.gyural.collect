<?

class dev_filtersCtrl extends standardController {

	function __construct() {

		$this->base = 'dev/filters/';
		$this->title = 'Filters';

		/* Filter only logged */
		$this->gyuFilter('before', function() { if(logged()) return false; return ['error' => 'You must be logged.']; }, 1, 'dev/filters/GetMustLog', ['GetOnly_logged']);
		
		/* Filter Injection */
		$this->gyuFilter('before', function() { $this->users = LoadClass('users', 1)->filter(); }, null, null, ['GetUsers']);
		$this->gyuFilter('before', 'dev/filters.preloadusers', null, null, ['GetUsers']);

		/* Filter Get */
		$this->gyuFilter('before', 'dev/filters.autoGet', 1, 'dev/filters/GetProvideId', ['GetSingle']);

	}

	function GetIndex() {
		$this->view('index');
	}

	/* Only Logged */

	function GetOnly_logged() {
		echo '<h1>You\'r logged. so you can see it.</h1>';
	}

	function GetMustLog() {
		echo '<pre>' . print_r($this->error, 1) . '</pre>';	
	}

	/* Injection */
	function GetUsers() {
		echo '<pre><strong>Injected anonymous function</strong>:' . "\n" .print_r($this->users,1) . "\n\n".'<strong>Method:</strong>' . "\n" .print_r($this->users_injected, 1).'</pre>';
	}

	function preloadusers($a) {
		$a->users_injected = LoadClass('users', 1)->filter();
		#return false;
	}

	/* Get */
	function GetSingle() {
		echo '<pre><strong>User Info</strong>:' . "\n" .print_r($this->user, 1).'</pre>';
	}

	function GetProvideId() {
		echo '<pre>' . print_r($this->error, 1) . '</pre>';
		echo '<a href="/dev/filters/single/user_id:1">Try</a>';
	}

	function autoGet($a) {
		if(!$_REQUEST["user_id"]) {

			return ['error' => 'No user provided.'];

		} else {

			if(!$u = LoadClass('users', 1)->get($_REQUEST["user_id"]))
				return ['error' => 'User not found'];

			$a->user = $u;

		}
	}

}