<?

class dev_uploadCtrl extends standardController {

	function __construct() {

		$this->base = 'dev/upload/';
		$this->title = 'Upload Class';

	}

	function GetIndex() {
		$this->view('index');
	}

	/* Example 1 */
	function GetExample1() {
		$this->view('example-form');
	}

	function PostExample1() {
		
		$config = [
			'upload_path' => upload,
			'allowed_types' => 'gif|jpg|png',
			'max_size' => 100,
			'max_width' => 1000,
			'max_height' => 1000,
		];
		
		$upload = new \Gyu\Upload($config);

		if(!$upload->do_upload('file')) {
			$this->move('dev/upload/GetExample1', ['errors' => $upload->error_msg]);
		} else {
			$data = array('upload_data' => $upload);
			echo '<pre>' . print_r($upload, 1);
		}

	}

	/* Example 2 */
	function GetExample2() {
		$this->view('example-form2');
	}

	function PostExample2() {
		
		$config_image = [
			'upload_path' => upload,
			'allowed_types' => 'gif|jpg|png',
			'max_size' => 100,
			'max_width' => 1000,
			'max_height' => 1000,
		];

		$config_pdf = [
			'upload_path' => upload,
			'allowed_types' => 'pdf'
		];
		
		$upload_image = new \Gyu\Upload($config_image);
		$upload_pdf = new \Gyu\Upload($config_pdf);

		$errors = array();

		if(!$upload_image->do_upload('image')) {
			array_push($errors, $upload_image->error_msg);
		} else {
			$_POST["image"] = $upload_image->file_name;
		}

		if(!$upload_pdf->do_upload('cv')) {
			array_push($errors, $upload_pdf->error_msg);
		} else {
			$_POST["cv"] = $upload_pdf->file_name;
		}

		$validate = new \Gyu\Validation($_POST);
		$validate->set_rules('name', 'Name', 'required|min_length[5]', ['min_length' => 'At least 5 chars']);
		$validate->set_rules('image', 'Image', 'required', ['required' => 'Image required.']);
		$validate->set_rules('cv', 'Pdf', 'required', ['required' => 'Pdf required.']);

		if(!$validate->run()) {
			$this->move('dev/upload/GetExample2', ['errors' => $validate->error_array(), 'content' => $validate->validation_data]);
		}

		echo '<pre>' . print_r($_POST, 1);


	}
	

}