<?

class devCtrl extends standardController {

	function __construct() {

		$this->title = 'Examples';
		$this->base = 'dev/';
		$this->sections_vendor = [
			'validation' => [
				'title' => 'Input Validation',
				'paragraphs' => [
					'index' => 'Examples',
					'filters' => 'Available Filters'
				]
			],
			'upload' => [
				'title' => 'File Upload',
				'paragraphs' => [
					'index' => 'Examples'
				]
			]
		];

		$this->sections_patterns = [
			'filters' => [
				'title' => 'GyuFilters',
				'paragraphs' => [
					'only_logged' => 'Only if logged',
					'users' => 'Injecting',
					'single' => 'If user_id passed'
				]
			]
		];

	}

	function GetIndex() {

		$this->view('index');

	}

}